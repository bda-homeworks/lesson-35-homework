package com.bda.lesson35.service;


import com.bda.lesson35.entity.Author;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import java.util.List;

public interface AuthorService {

    List<Author> findAllAuthors();

    Author findByAuthorId(Long id);

    void createAuthor(Author author);

    void updateAuthor(Author author);

    void deleteAuthor(Long id);
}
