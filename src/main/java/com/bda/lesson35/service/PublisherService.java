package com.bda.lesson35.service;


import com.bda.lesson35.entity.Publisher;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import java.util.List;
public interface PublisherService {

    List<Publisher> findAllPublishers();

    Publisher findPublisherById(Long id);

    void createPublisher(Publisher publisher);

    void updatePublisher(Publisher publisher);

    void deletePublisher(Long id);
}
