package com.bda.lesson35.service.Impl;

import com.bda.lesson35.entity.Author;
import com.bda.lesson35.exception.NotFoundException;
import com.bda.lesson35.repository.AuthorRepository;
import com.bda.lesson35.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class AuthorServiceImpl implements AuthorService {

    @Autowired
    private final AuthorRepository authorRepo;

    public AuthorServiceImpl(AuthorRepository authorRepo) {
        this.authorRepo = authorRepo;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    @Override
    public List<Author> findAllAuthors() {
        return authorRepo.findAll();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    @Override
    public Author findByAuthorId(Long id) {
        return authorRepo.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Author not found with ID %d", id)));
    }

    @Override
    public void createAuthor(Author author) {
        authorRepo.save(author);
    }

    @Override
    public void updateAuthor(Author author) {
        authorRepo.save(author);
    }

    @Override
    public void deleteAuthor(Long id) {
        final Author author = authorRepo.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format("Author not found with ID %d")));

        authorRepo.deleteById(author.getId());
    }
}
