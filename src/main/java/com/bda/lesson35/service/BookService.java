package com.bda.lesson35.service;

import com.bda.lesson35.entity.Book;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import java.util.List;

public interface BookService {

    List<Book> findAllBooks();

    List<Book> searchBooks(String keyword);

    Book findBookById(Long id);

    void createBook(Book book);

    void updateBook(Book book);

    void deleteBook(Long id);
}
