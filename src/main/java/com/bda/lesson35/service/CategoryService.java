package com.bda.lesson35.service;

import com.bda.lesson35.entity.Category;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import java.util.List;

public interface CategoryService {

    List<Category> findAllCategories();

    Category findCategoryById(Long id);

    void createCategory(Category category);

    void updateCategory(Category category);

    void deleteCategory(Long id);
}
