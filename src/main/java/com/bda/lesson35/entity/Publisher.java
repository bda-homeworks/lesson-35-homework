package com.bda.lesson35.entity;


import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.HashSet;
import java.util.Set;


@Data
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "publishers")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Publisher {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "name", length = 100, nullable = false)
    String name;

    @ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "publishers")
    Set<Book> books = new HashSet<>();

    public Publisher(String name, String description) {
        this.name = name;
    }

}
