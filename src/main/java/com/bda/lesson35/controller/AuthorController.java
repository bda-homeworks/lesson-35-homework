package com.bda.lesson35.controller;

import com.bda.lesson35.entity.Author;
import com.bda.lesson35.service.AuthorService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class AuthorController {

    private final AuthorService service;

    public AuthorController(AuthorService authorService) {
        this.service = authorService;
    }

    @RequestMapping("/authors")
    public String findAllAuthors(Model model) {
        final List<Author> authors = service.findAllAuthors();

        model.addAttribute("authors", authors);
        return "list-authors";
    }

    @RequestMapping("/authors/{id}")
    public String findAuthorById(@PathVariable("id") Long id, Model model) {
        final Author author = service.findByAuthorId(id);

        model.addAttribute("author", author);
        return "list-author";
    }

    @GetMapping("/addAuthor")
    public String showCreateForm(Author author) {
        return "add-author";
    }

    @RequestMapping("/add-author")
    public String createAuthor (Author author, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-author";
        }

        service.createAuthor(author);
        model.addAttribute("author", service.findAllAuthors());
        return "redirect:/authors";
    }

    @GetMapping("/updateAuthor/{id}")
    public String showUpdateForm(@PathVariable("id") Long id, Model model) {
        final Author author = service.findByAuthorId(id);

        model.addAttribute("author", author);
        return "update-author";
    }

    @RequestMapping("/update-author/{id}")
    public String updateAuthor(@PathVariable("id") Long id, Author author, BindingResult result, Model mdodel) {
        if (result.hasErrors()) {
            author.setId(id);
            return "update-author";
        }

        service.updateAuthor(author);
        mdodel.addAttribute("author", service.findAllAuthors());
        return "redirect:/authors";
    }

    @RequestMapping("/remove-author/{id}")
    public String deleteAuthor(@PathVariable("id") Long id, Model model) {
        service.deleteAuthor(id);

        model.addAttribute("author", service.findAllAuthors());
        return "redirect:/authors";
    }
}
